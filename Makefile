all: pacman

CC = g++ -std=c++17
WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O2
SRC = $(shell find src/*.cpp)
OBJ = $(subst src,dist,$(SRC:.cpp=.o))
CFLAGS=-Iinclude
LIBS=-lSDL2

pacman: $(OBJ)
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) $^ $(LIBS)

dist/%.o: src/%.cpp
	$(CC) -c -o $@ $^ $(DEBUG) $(WARNINGS) $(OPTIMIZE) $(CFLAGS)

clean:
	rm -f pacman dist/*

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./pacman

