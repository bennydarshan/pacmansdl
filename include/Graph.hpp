#include <utility>
#include<vector>

class Graph{ // TODO: Maybe use a typedef for the pair?
private:
	std::vector<std::pair<int,int>> vertecies;
	bool **edges = nullptr;
	int veID(const std::pair<int, int> &entry) const;
	std::pair<int, int> getVert(int id) const { return vertecies[id];}
	void SetEdges();
public:
	//Graph(const std::vector<int> &map, int w);
	Graph(){};
	Graph(std::vector<std::pair<int, int>> &v);
	~Graph();
	void AddVertex(std::pair<int, int> entry){ vertecies.emplace_back(entry);} //TODO: maybe using a lvalue byref makes more sense here?
	void InitEdges();
	void AddEdge(const std::pair<int, int> &entry1, const std::pair<int, int> &entry2){
		if(!edges) return; //Shouldn't happen
		const int entry1_id = veID(entry1);
		const int entry2_id = veID(entry2);
		edges[entry1_id][entry2_id] = true;
		edges[entry2_id][entry1_id] = true;
	}
	std::vector<std::pair<int, int>> Neighbors(std::pair<int, int> entry);

	enum dir{LEFT, RIGHT, UP, DOWN};

	class Iterator{
	private:
		const Graph &super;
		int index;
	public:
		Iterator(const Graph &g, int i): super(g), index(i){}
		Iterator(const Graph &g, std::pair<int, int> v): super(g){
			index = super.veID(v);
		}
		void move(Graph::dir d);

	};
};