#pragma once
#include <SDL2/SDL.h>

#include "Map.hpp"

class Game{
private:
	Map *map = nullptr;
	SDL_Window *win = nullptr;
	SDL_Renderer *ren = nullptr;
	bool running = true;
	void EventHandle();
public:
	Game(){
		SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO);
		win = SDL_CreateWindow("Pacman", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 720, 1280, SDL_WINDOW_OPENGL);
		ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
		Map::SetReneder(ren);
		Map::SetTexture("res/maptiles.bmp");
	}
	~Game(){
		if(map) delete map;
		SDL_DestroyRenderer(ren);
		SDL_DestroyWindow(win);
		SDL_Quit();
	}
	void Loop();
	void GenMap(const char *file){
		map = new Map(file);
	}
};