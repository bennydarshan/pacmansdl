#pragma once

#include <SDL2/SDL.h>
#include <vector>

#include "Graph.hpp"

class Map{

private:
	int w, h;
	std::vector<int> data;
	Graph graph;
	static SDL_Texture *sprites; // FIXME: since we are going to generate the texture on creation, this can be a local variable for the c'tor function.
	static SDL_Renderer *ren; // Todo: Create a generic object that will hold a renderer.
	static SDL_Rect gettile(int num) // This should be in the scope of any instance for map.
	{
		const int w = 12;
		const int h = 12;

		int x = num%16*w;
		int y = num/16*h;
		SDL_Rect ret = {x, y, w, h };
		return ret;


	}
	std::pair<int, int> fromIndex(int i){
		return {i%w, i/w};
	}

public:
	Map(const char *file);
	void Draw(); //TODO: instead of generating the map each time, we can generate a cached texture and have the map draw that.
	void genGraph();

	static void SetReneder(SDL_Renderer *exren){
		ren = exren;
	}

	static void SetTexture(const char *file){
		auto rawTex = SDL_LoadBMP(file);
		sprites = SDL_CreateTextureFromSurface(ren, rawTex);
		SDL_FreeSurface(rawTex);
	}

	Graph::Iterator pacmanPosition()
	{
		for(size_t i = 0; i< data.size; i++) {
			if(data[i] == -1) return {graph, {i%w, i/w}};
		}
		std::cout << "Probably a bugged map\n";
		return { graph, 0};
	}

};