#include "Graph.hpp"


Graph::Graph(std::vector<std::pair<int, int>> &v): vertecies(v)
{
	InitEdges();
}

Graph::~Graph()
{
	if(!edges) return;
	for(size_t i = 0;i<vertecies.size();i++)
		delete[] edges[i];
	delete[] edges;
}

int Graph::veID(const std::pair<int, int> &entry) const
{
	for(size_t i=0;i<vertecies.size();i++)
		if(entry == vertecies[i]) return i;
	return -1;
}

void Graph::InitEdges()
{
		const auto _size = vertecies.size();
		edges = new bool*[_size];
		for(size_t i = 0;i < _size;i++)
			edges[i] = new bool[_size];
		

}

void Graph::SetEdges()
{
	for(auto &v : vertecies) {
		auto _cur_index = veID(v);
		// For each we need to check, left, right, up, down.
		auto _temp = v;
		_temp.first++;
		if(auto _temp_index = veID(_temp) >= 0) {
			edges[_cur_index][_temp_index] = true;
			edges[_temp_index][_cur_index] = true;
		}

	}

}

std::vector<std::pair<int, int>> Graph::Neighbors(std::pair<int, int> entry)
{ // FIXME: maybe we should just keep a vector of ints?
	const int entry_id = veID(entry);
	std::vector<std::pair<int, int>> ret;
	for(size_t i = 0;i<vertecies.size();i++)
		if(edges[entry_id][i]) ret.emplace_back(getVert(i));
	
	return ret;

}

void Graph::Iterator::move(Graph::dir d){
	std::pair<int, int> check;
	check = super.getVert(index);
	switch(d) {
		case LEFT:
			check.first--;
			break;
		case RIGHT:
			check.first++;
			break;
		case UP:
			check.second--;
			break;
		case DOWN:
			check.second++;
			break;

	}	
	auto ret = super.veID(check);
	if(ret >= 0) index = ret; 
}