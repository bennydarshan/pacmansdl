#include "Game.hpp"

void Game::EventHandle()
{
	SDL_Event e;
	while(SDL_PollEvent(&e))
		switch(e.type){
			case SDL_QUIT:
				running = false;
				break;
			//TODO: Implement keyboard strokes handlers
		}
}

void Game::Loop()
{
	while(running){
		EventHandle();

		SDL_RenderClear(ren);
		
		/* drawing take place here */
		if(map) map->Draw();
		SDL_RenderPresent(ren);
	}
}