#include <fstream>
#include <iostream>
#include <string>

#include "Map.hpp"
SDL_Texture *Map::sprites = nullptr;
SDL_Renderer *Map::ren = nullptr;

Map::Map(const char *file)
{
	std::ifstream in;
	std::string entry;
	std::vector<std::pair<int, int>> v;
	in.open(file);
	std::getline(in, entry, ',');
	w = std::stoi(entry);
	std::getline(in, entry, '\n');
	h = std::stoi(entry);

	while(std::getline(in, entry, ','))
	{
		//std::cout << entry << ", ";
		data.emplace_back(std::stoi(entry));
	}
	/*for(auto tile : data){
		std::cout << tile << ", ";
	}
	std::cout << std::endl;*/
	for(size_t i = 0; i < data.size; i++) {
		if(data[i] == 0) v.push_back({i%w, i/w});
	}
	graph = Graph(v);

}

void Map::Draw()
{
	const auto stile = 16;
	SDL_Rect _r = {0, 0, stile, stile};
	for(size_t i=0;i<data.size();i++)
	{
		_r.x = i%w * stile;
		_r.y = i/w * stile;
		if(!data[i]) continue;
		//std::cout << "{ " << _r.x << " ," << _r.y << " }\t";
		SDL_Rect _d = gettile(data[i] - 1);
		SDL_RenderCopy(ren, sprites, &_d, &_r); 
	}
	//std::cout << std::endl;
}

void Map::genGraph()
{
	for(size_t i = 0;i < data.size(); i++)
	{
		if(!data[i])
			graph.AddVertex(fromIndex(i));
	}
	graph.InitEdges();
	for(size_t i = 0; i < data.size(); i++)
	{
		if(data[i]) continue;
		const std::pair<int, int> cur = fromIndex(i);
		//FIXME: the entry might be at the edge? (Probably not, because then we should have a wall, worth looking at the math for it though.)
		if(!data[i - 1]) graph.AddEdge(cur, fromIndex(i - 1));
		if(!data[i + 1]) graph.AddEdge(cur, fromIndex(i + 1));
		if(!data[i + w]) graph.AddEdge(cur, fromIndex(i + w));
		if(!data[i - w]) graph.AddEdge(cur, fromIndex(i - w));

	}
}